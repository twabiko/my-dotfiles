#!/bin/bash

set -eu

# スニペットを作る
$HOME/.vim/bundles/repos/github.com/pearofducks/ansible-vim/UltiSnips/generate.py --output $HOME/.vim/bundles/.cache/.vimrc/.dein/UltiSnips/ansible.snippets --style dictionary
