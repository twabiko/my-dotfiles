﻿# installer.ps1

Set-StrictMode -Version Latest

# 管理者権限があるか確認する
if (-not (([Security.Principal.WindowsPrincipal] `
    [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
        [Security.Principal.WindowsBuiltInRole] "Administrator"))) {
    Write-Warning "管理者権限で実行してください"
    exit 1
}

# ローカルリポジトリのパスを得る
$Path = Join-Path (Split-Path -Parent $MyInvocation.MyCommand.Path) ".."
$LocalRepoDir = [System.IO.Path]::GetFullPath($Path)
$SrcDir = Join-Path $LocalRepoDir "src"

# ファイルのシンボリックリンクを上書きで作る
New-Item -Force -Path $HOME -Name .ctags `
    -Value $SrcDir/dot.ctags -ItemType SymbolicLink
New-Item -Force -Path $HOME -Name .editorconfig `
    -Value $SrcDir/dot.editorconfig -ItemType SymbolicLink
New-Item -Force -Path $HOME -Name .gitconfig `
    -Value $SrcDir/dot.gitconfig -ItemType SymbolicLink
New-Item -Force -Path $HOME -Name .gitconfig_windows.inc `
    -Value $SrcDir/dot.gitconfig_windows.inc -ItemType SymbolicLink
New-Item -Force -Path $HOME -Name _gvimrc `
    -Value $SrcDir/dot.gvimrc -ItemType SymbolicLink
New-Item -Force -Path $HOME -Name .vimrc `
    -Value $SrcDir/dot.vimrc -ItemType SymbolicLink

# ディレクトリのシンボリックリンクを上書きで作る
New-Item -Force -Path $HOME -Name .vim `
    -Value $SrcDir/dot.vim -ItemType SymbolicLink
New-Item -Force -Path $HOME\AppData\Local -Name nvim `
    -Value $SrcDir/dot.config\nvim -ItemType SymbolicLink

# 設定サンプルがなければコピーする
if (-not(Test-Path $HOME/.gitconfig_local.inc)) {
    Copy-Item $SrcDir/dot.gitconfig_local.inc $HOME/.gitconfig_local.inc
}

# dein.vim をインストールする
if ((Get-Command gvim -ErrorAction SilentlyContinue) -and `
    (Get-Command git -ErrorAction SilentlyContinue)) {
    $DeinPluginDir = "$HOME\.vim\bundles"
    $DeinInstallDir = "$DeinPluginDir\repos\github.com\Shougo\dein.vim"
    if (-not (Test-Path "$DeinInstallDir")) {
        $Url = "https://github.com/Shougo/dein.vim"
        # vim7.4 の場合
        #git clone $Url "$DeinInstallDir" -b 1.5
        git clone $Url "$DeinInstallDir"
    }
}
