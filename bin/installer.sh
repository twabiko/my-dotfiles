#!/bin/bash

set -eu

option_vim7=no

usage_exit() {
  echo "usage: $0 [options...]"
  echo ""
  echo "options:"
  echo "  --vim7, -V: use vim7"
  exit 1
}

while getopts -- "-:hV" opt; do
  case $opt in
    -)
      case $OPTARG in
        vim7) option_vim7=yes;;
        *)
          echo_err_badopt $OPTARG
          usage_exit
          ;;
      esac;;
    V)  option_vim7=yes;;
    h)  usage_exit;;
    \?) usage_exit;;
  esac
done
shift $((OPTIND-1))

# ローカルリポジトリのパスを得る
local_repo_dir=$(readlink -f $(dirname $0)/..)
src_dir=$local_repo_dir/src

mkdir -pv $HOME/.config
mkdir -m 0700 -pv $HOME/.ssh

# ファイルのシンボリックリンクを上書きで作る
ln -fsv $src_dir/dot.bashrc_local.sh $HOME/.bashrc_local.sh
ln -fsv $src_dir/dot.editorconfig $HOME/.editorconfig
ln -fsv $src_dir/dot.gitconfig $HOME/.gitconfig
ln -fsv $src_dir/dot.gvimrc $HOME/.gvimrc
ln -fsv $src_dir/dot.inputrc $HOME/.inputrc
ln -fsv $src_dir/dot.tmux.conf $HOME/.tmux.conf
ln -fsv $src_dir/dot.vimrc $HOME/.vimrc
ln -fsv $src_dir/dot.ctags $HOME/.ctags

# ディレクトリのシンボリックリンクを上書きで作る
ln -fnsv $src_dir/dot.bashrc.d $HOME/.bashrc.d
ln -fnsv $src_dir/dot.vim $HOME/.vim
ln -fnsv $src_dir/dot.config/nvim $HOME/.config/nvim

# なければコピーする
cp -nv $src_dir/dot.gitconfig_local.inc \
  $HOME/.gitconfig_local.inc
[[ -e $HOME/.ssh/config ]] && \
  ln -nsv $src_dir/dot.ssh/config $HOME/.ssh/config

# $HOME/.bashr_local.sh を読むようにする
line='source $HOME/.bashrc_local.sh'
grep -F "$line" $HOME/.bashrc >/dev/null || {
  cp -aiv $HOME/.bashrc{,-$(date "+%F-%s")}
  ( echo ""; echo "$line" ) | tee -a $HOME/.bashrc >/dev/null
}

# dein.vim をインストールする
type vim >/dev/null && type git >/dev/null && {
  dein_install_dir=$HOME/.vim/bundles
  if [[ ! -d $dein_install_dir ]]; then
    tmp_dir=$(mktemp -d)
    pushd $tmp_dir
    url=https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh
    curl "$url" >installer.sh
    sh ./installer.sh $dein_install_dir
    popd
  fi
} || true

# vim7 環境のためにコメントアウトしておく
if [[ $option_vim7 == yes ]]; then
  # 必要条件が vim8 となる直前のコミットに巻き戻す
  pushd $dein_install_dir/repos/github.com/Shougo/dein.vim
  git checkout 157a8f9e413fcfe89000fd62f115d9d7351edff9
  popd
fi
