﻿# start-powershell-runas.ps1

Set-StrictMode -Version Latest

$path = Split-Path -Parent $MyInvocation.MyCommand.Path
$args = ("-NoExit", "-Command", "cd $path")

Start-Process powershell.exe $args -Verb runas
