#!/bin/bash

set -eu

# ローカルリポジトリのパスを得る
local_repo_dir=$(readlink -f $(dirname $0)/..)
dest_dir=$local_repo_dir/src/dot.bashrc.d

# ファイルを得る
url_base=https://raw.githubusercontent.com/git/git/master/contrib/completion
curl $url_base/git-prompt.sh \
  | tee $dest_dir/90-git-prompt.sh.disabled >/dev/null
curl $url_base/git-completion.bash \
  | tee $dest_dir/90-git-completion.sh.disabled >/dev/null
