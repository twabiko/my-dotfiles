# README

## 要件

* vim8 or nvim

## ノート

CentOS7 の場合、 vim が 7系なので動作対象外。
ただし、 nvim を加えるか、dein を旧コミットまで巻き戻せば、
利用可能ではあるはず。
