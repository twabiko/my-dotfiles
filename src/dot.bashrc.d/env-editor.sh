# env-editor.sh
if type vim >/dev/null 2>&1; then
  export EDITOR=vim
else
  if type vi >/dev/null 2>&1; then
    export EDITOR=vi
  fi
fi
