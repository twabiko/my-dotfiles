# env-lang.sh
if [[ ! $LANG =~ ^en_ ]]; then
  export LANG=en_US.UTF-8
fi
