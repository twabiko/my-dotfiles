# .gitconfig_windows.inc
[core]
	editor = gvim -c \"set fenc=utf-8\"

[color]
	diff=false
	status=false
	branch=false
	interactive=false
