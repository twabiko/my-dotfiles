" .vimrc

let s:FONT_SIZE_SMALL = 10
let s:FONT_SIZE_NORMAL = 12
let s:FONT_SIZE_LARGE = 16
let s:FONT_SIZE_EXLARGE = 20
let s:FONT_SIZE_DEFAULT = s:FONT_SIZE_NORMAL

let s:OS_TYPE_UNKNOWN = -1
let s:OS_TYPE_WINDOWS = 1
let s:OS_TYPE_LINUX = 2

let s:enable_tmux = 0
let g:enable_devicons = 0

" フォントサイズを設定する
function! s:MySetGuiFont(font_size)
  let &guifont = 'Cica:h' . a:font_size . ':cSHIFTJIS:qDRAFT'
endfunction
command! MyFontSizeSmall   call s:MySetGuiFont(s:FONT_SIZE_SMALL)
command! MyFontSizeNormal  call s:MySetGuiFont(s:FONT_SIZE_NORMAL)
command! MyFontSizeLarge   call s:MySetGuiFont(s:FONT_SIZE_LARGE)
command! MyFontSizeExLarge call s:MySetGuiFont(s:FONT_SIZE_EXLARGE)

" インデントガイドが無効か確認する
function! s:MyIndentGuideDisabled()
  call indent_guides#init_matches()
  return empty(w:indent_guides_matches)
endfunction

" インデントガイドが有効か確認する
function! s:MyIndentGuideEnabled()
  return ! s:MyIndentGuideDisabled()
endfunction

" インデントガイドをリロードする
function! MyIndentGuideReload()
  if s:MyIndentGuideEnabled()
    IndentGuidesDisable
    IndentGuidesEnable
  endif
endfunction
command! MyIndentGuideReload call MyIndentGuideReload()

" タブストップを変える
function! s:MySetLocalTabstop(ts)
  let &l:tabstop = a:ts
  let &l:shiftwidth = a:ts
  let &l:softtabstop = a:ts
  setlocal expandtab
  call MyIndentGuideReload()
endfunction
command! MyTabstopWidth2 call s:MySetLocalTabstop(2)
command! MyTabstopWidth3 call s:MySetLocalTabstop(3)
command! MyTabstopWidth4 call s:MySetLocalTabstop(4)

" ReStructuredText用の設定にする
function! MyFileTypeRst()
  setlocal filetype=rst
  call s:MySetLocalTabstop(3)
endfunction
command! MyFileTypeRst call MyFileTypeRst()

" Markdown編集用の設定にする
function! MyFileTypeMarkdown()
  setlocal filetype=markdown
  call s:MySetLocalTabstop(3)
endfunction
command! MyFileTypeMarkdown call MyFileTypeMarkdown()

" カーソル行の直後にmodelineを加える
function! MyModelineAppend()
  let pos = getpos(".")

  let modeline = "vim:set ft=" . &filetype . ":"

  let line = ""
  if &filetype == ""
    return
  elseif &filetype == "markdown"
    let line = "<!-- " . modeline . " -->"
  elseif &filetype == "rst"
    let line = ".. " . modeline
  else
    let line = modeline
  endif
  execute ":normal A\n" . line

  call setpos(".", pos)
endfunction
command! MyModelineAppend call MyModelineAppend()

" Tagbar をリロードする
function! MyTagbarReload()
  if exists('t:tagbar_buf_name') && bufwinnr(t:tagbar_buf_name) != -1
    TagbarClose
    TagbarOpen
  endif
endfunction
command! MyTagbarReload call MyTagbarReload()

" 色を設定する
function! MySolarized()
  let g:solarized_termcolors=256
  set background=dark
  colorscheme solarized
endfunction
command! MySolarized call MySolarized()

" OSを判定する
if has('win32')
  let s:os_type = s:OS_TYPE_WINDOWS
elseif substitute(system('uname'), '\n', '', '') == 'Linux'
  let s:os_type = s:OS_TYPE_LINUX
else
  let s:os_type = s:OS_TYPE_UNKNOWN
endif

" 有効とする機能を選ぶ
if s:os_type == s:OS_TYPE_LINUX
  let s:enable_tmux = 1
endif
if s:os_type == s:OS_TYPE_WINDOWS && has('gui_running')
  let g:enable_devicons = 1
endif

" Python3の設定をする
if s:os_type == s:OS_TYPE_WINDOWS && !has('nvim')
  set pythonthreedll=C:\Python36\python36.dll
endif

" ビジュアルベルを無効にする
set novisualbell

" 既存ファイル編集時のエンコーディング候補順序を定める
set fileencodings=utf-8,cp932,utf-16le

" ファイル再オープン時にカーソルを移動する (:help restore-cursor)
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif

if has('gui_running')
  " プリント時に文字化けしたため、これを回避するために追加した
  call s:MySetGuiFont(s:FONT_SIZE_DEFAULT)

  " 選択範囲を他のアプリケーションでペーストできるようにする
  set guioptions+=a

  " ツールバーを非表示にする
  set guioptions-=T

  " insert モードで IM を OFF にする
  set iminsert=0

  " 検索パタンを入力時、 IM を iminsert 設定値で設定する
  set imsearch=-1
endif

" 行番号を表示させる
set number

" カーソル行を表示させる
set cursorline

" 長い行を入力するときvimが改行を入れないようにする
set textwidth=0

" 折り畳み行の行頭に視覚的なインデントを入れる
if v:version > 704
  set breakindent
endif

" カーソルの上部または下部で表示するスクリーン行数の最小数を指定する
set scrolloff=5

" 置換時に置換結果のプレビューをプレビューウィンドウで表示する
if has('nvim')
  set inccommand=split
endif

" 検索キーワードを強調させない
set nohlsearch

" バックアップファイル (*~) を作らない
set nobackup

" 同一ファイルを複数vimで開いたときに気づけるようにするため、
" スワップファイルを作る
set swapfile

" Windows環境では次のパスにスワップを作る
if s:os_type == s:OS_TYPE_WINDOWS
  set directory=$TEMP,c:\\tmp,c:\\temp,.
endif

" undo ファイル (un~) を作らない
set noundofile

" プラグインマネージャを設定する
let s:my_dein_install_dir = $HOME."/.vim/bundles"
let s:my_dein_cache_dir = s:my_dein_install_dir . "/.cache/.vimrc/.dein"
let s:my_dein_vim_dir =
\     s:my_dein_install_dir . "/repos/github.com/Shougo/dein.vim"

"dein Scripts-----------------------------
if &compatible
  set nocompatible
endif
execute "set runtimepath+=".escape(s:my_dein_vim_dir, ' ')
if dein#load_state(s:my_dein_install_dir)
  call dein#begin(s:my_dein_install_dir)

  call dein#add(s:my_dein_install_dir)
  call dein#load_toml($HOME."/.vim/rc/dein.toml")
  if has('python3')
    " UltiSnips is the ultimate solution for snippets in Vim.
    call dein#add('SirVer/ultisnips')
  endif
  if s:os_type == s:OS_TYPE_LINUX
    " A vim plugin to give you some slime.
    call dein#add('jpalardy/vim-slime')
  endif

  call dein#end()
  call dein#save_state()
endif

filetype plugin indent on
syntax enable
"End dein Scripts-------------------------

if dein#check_install()
  call dein#install()
endif

" モジュール読み込み後に変数を上書きするため、ランタイムパスを加える
execute "set runtimepath+=".escape($HOME."/.vim/after", ' ')

colorscheme apprentice " カラースキームを設定する

"-----------------------------------------
" submode

" ウィンドウの大きさを C-w <>+- で変更する
call submode#enter_with('winsize', 'n', '', '<C-w>>', '<C-w>>')
call submode#enter_with('winsize', 'n', '', '<C-w><', '<C-w><')
call submode#enter_with('winsize', 'n', '', '<C-w>+', '<C-w>+')
call submode#enter_with('winsize', 'n', '', '<C-w>-', '<C-w>-')
call submode#map('winsize', 'n', '', '>', '<C-w>>')
call submode#map('winsize', 'n', '', '<', '<C-w><')
call submode#map('winsize', 'n', '', '+', '<C-w>+')
call submode#map('winsize', 'n', '', '-', '<C-w>-')

" 使い方
" vim
" :vsplit
" :split
" C-w +++---
" C-w >>><<<

"-----------------------------------------
" NERDTree

let NERDTreeShowBookmarks = 1   " 開始時にブックマークを表示
let NERDTreeShowHidden = 1      " 隠しファイルを表示
let NERDTreeSortHiddenFirst = 1 " 隠しファイルは先頭に表示

" ディレクトリの矢印を指定する
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" 使い方
" :NERDTree

"-----------------------------------------
" VimFiler

" 使い方
" :VimFiler

"-----------------------------------------
" unite

" 使い方
" :Unite file buffer

"-----------------------------------------
" ctrlp.vim

" 使い方
" C-p 後に編集したいファイル名の一部を入力する

"-----------------------------------------
" vim-devicons

set encoding=utf8    " vim内で使うエンコーディングを指定する
set ambiwidth=double " East Asian Width Class Ambiguous を2文字幅で扱う

if g:enable_devicons
  " gvim で encoding=utf8 かつメニューを日本語で表示させると、
  " 表示がおかしくなるため、英語表示とする。
  set langmenu=en_US
  source $VIMRUNTIME/delmenu.vim
  source $VIMRUNTIME/menu.vim

  " フォルダアイコンの表示をON
  let g:WebDevIconsUnicodeDecorateFolderNodes = 1

  " NERDTree 使用時、ブラケット記号 [] を不可視にする
  if exists("g:loaded_webdevicons")
      call webdevicons#refresh()
  endif
endif

"-----------------------------------------
" bufexplorer

" 使い方
" :BufExplorer

"-----------------------------------------
" lightline.vim

set laststatus=2 " 常にステータスを表示させる
let g:lightline = {
  \ 'colorscheme': 'jellybeans',
  \ 'active': {
  \   'left': [ [ 'paste' ],
  \             [ 'fugitive', 'filename' ] ]
  \ },
  \ 'component_function': {
  \   'fugitive': 'LightLineFugitive',
  \   'readonly': 'LightLineReadonly',
  \   'modified': 'LightLineModified',
  \   'filename': 'LightLineFilename',
  \   'filetype': 'LightLineFiletype',
  \   'fileformat': 'LightLineFileformat',
  \   'lineinfo': 'LightLineLineinfo',
  \ },
  \ 'separator': { 'left': '' },
  \ 'subseparator': { 'left': '|', 'right': '|' }
  \ }

function! LightLineFugitive()
  if exists("*fugitive#head")
    let _ = fugitive#head()
    return strlen(_) ? (g:enable_devicons ? '' : '') . _ : ''
  endif
  return ''
endfunction

function! LightLineReadonly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return g:enable_devicons ? "" : "[RO]"
  else
    return ""
  endif
endfunction

function! LightLineModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction

function! LightLineFilename()
  return ('' != LightLineReadonly() ? LightLineReadonly().' ' : '').
    \ ('' != expand('%:t') ? expand('%:t') : '[No Name]').
    \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
endfunction

function! LightLineFiletype()
  return winwidth(0) > 70 ?
    \ (strlen(&filetype) ? &filetype . (g:enable_devicons ?
    \   ' ' . WebDevIconsGetFileTypeSymbol() : '') : 'no ft')
    \ : ''
endfunction

function! LightLineFileformat()
  return winwidth(0) > 70 ?
    \ (&fileformat . (g:enable_devicons ?
    \   ' ' . WebDevIconsGetFileFormatSymbol() : ''))
    \ : ''
endfunction

function! LightLineLineinfo()
  " カーソル位置を得る
  let _ = getpos(".")

  " ウィンドウの幅を得る
  redir =>a |exe "sil sign place buffer=".bufnr('')|redir end
  let signlist = split(a, '\n')
  let width = winwidth(0) -
    \ ((&number||&relativenumber) ?  &numberwidth : 0) -
    \ &foldcolumn -
    \ (len(signlist) > 2 ? 2 : 0)

  return winwidth(0) > 40 ?
    \ printf("%3d:%2d/%2d", _[1], _[2], width) :
    \ printf("%d:%-d", _[1], _[2])
endfunction

"-----------------------------------------
" vim-indent-guides

let g:indent_guides_enable_on_vim_startup = 1 " vim起動時に有効にする
let g:indent_guides_guide_size = 1            " サイズは1文字で表現する

" nerdtree は除外する
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']

" 使い方
" <Leader>ig --> インデントガイドの有効無効を切り替える
" なお、<Leader> のデフォルトは \ キー

"-----------------------------------------
" vim-quickrun

" 使い方
" :QuickRun<enter> でバッファ内を実行すると結果が別バッファで得られる。
" Python 等のコードを書いて実行する等が便利。

"-----------------------------------------
" vim-slime

" 使い方
" $ tmux
" C-b :split-window         --> 画面が水平2分割される
" C-b q                     --> 画面の番号を確認
" vim
" idate<ESC>                --> 1行目に date を書く
" C-c C-c
" tmux socket name: default
" tmux target pane: :0.0    --> 初回はどの pane に出力するか決める
"                           --> date が実行される
" C-c C-c                   --> 2回目以降は pane 指定不要となる

"-----------------------------------------
" vim-surround

" Visualモードで範囲を選択して S + '記号' と入力すると、記号で括る
" vim
" iThis is a selected text.<ESC>
" /a<enter>
" v/t<enter>nn
" S"

"-----------------------------------------
" vim-fugitive

" コミットログを確認する
"   :Glog | copen
"     --> quickfix にログを表示
" キーワード検索する
"   :Ggrep 検索したいワード | copen
"     --> quickfix に検索結果を表示
" 編集箇所がどのコミットか確認する
"   :Gblame
" コミットするファイルを選ぶ
"   :Gstatus
"     --> 行選択して、 - で add/reset に。 p でパッチ表示。
" コミット予定の差分を確認する
"   :Git! diff --cached
"     --> git diff --cached
"   :Greview
" コミットする
"   :Gcommit
" プッシュする
"   :Gpush

"-----------------------------------------
" vim-snippets

" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" 使い方
" $ vim hoge.c
" main<tab>
"   --> main 関数のひな型作られる
" $ vim hoge.yml
" :set ft=ansible
" lineinfile<tab>

" 他にもスニペット管理があるが、
" ansible-vim のディレクトリ名にあわせて UltiSnips を選択した。

"-----------------------------------------
" ansible-vim

let g:ansible_unindent_after_newline = 1
let g:ansible_attribute_highlight = "ob"
let g:ansible_name_highlight = 'd'
let g:ansible_extra_keywords_highlight = 1
let g:ansible_normal_keywords_highlight = 'Constant'


" 初期設定:
" $ sudo yum install epel-release -y
" $ sudo yum-config-manager --disable epel
" $ sudo yum --enablerepo=epel install ansible -y
" $ cd ~/.vim/bundles/repos/github.com/pearofducks/ansible-vim/UltiSnips
" $ ./generate.py

" 使い方:
" $ mkdir -pv /tmp/playbooks
" $ vim /tmp/playbooks/hoge.yml
" i- name: hoge<enter>lineinfile<tab>
"   --> lineinfile の引数が列挙される

"-----------------------------------------
" vim-markdown

let g:vim_markdown_folding_disabled = 1

"-----------------------------------------
" tagbar

let g:tagbar_type_ansible = {
  \   'ctagstype' : 'ansible',
  \   'kinds' : [ 't:tasks' ],
  \   'sort' : 0
  \ }

let g:tagbar_type_rst = {
  \   'ctagstype': 'rst',
  \   'ctagsbin' : g:dein#_runtime_path . '/rst2ctags.py',
  \   'ctagsargs' : '-f - --sort=yes',
  \   'kinds' : [ 's:sections', 'i:images' ],
  \   'sro' : '|',
  \   'kind2scope' : { 's' : 'section' },
  \   'sort': 0,
  \ }

let g:tagbar_type_ps1 = {
  \   'ctagstype': 'powershell',
  \   'kinds': [
  \     'f:function',
  \     'i:filter',
  \     'a:alias'
  \   ]
  \ }

let g:tagbar_type_markdown = {
  \   'ctagstype': 'markdown',
  \   'ctagsbin': s:my_dein_cache_dir . '/markdown2ctags.py',
  \   'ctagsargs': '-f - --sort=yes',
  \   'kinds' : [
  \     's:sections',
  \     'i:images'
  \   ],
  \   'sro' : '|',
  \   'kind2scope' : {
  \     's' : 'section',
  \   },
  \   'sort': 0,
  \ }

"-----------------------------------------
" vim-markdown

"-----------------------------------------
" vim-flake8

let g:flake8_show_in_gutter=1

" 使い方:
" $ vim x.py
" F7
"   --> flake8 が実行され QuickFix に表示される

"-----------------------------------------

" ファイルタイプを定める

au BufRead,BufNewFile */playbooks/*.yml set filetype=yaml.ansible
au BufRead,BufNewFile */ansible/*.yml set filetype=yaml.ansible

au BufRead,BufNewFile */dot.gitconfig_local.inc set filetype=conf
au BufRead,BufNewFile */.gitconfig_local.inc set filetype=conf

au BufRead,BufNewFile *.md set filetype=markdown
